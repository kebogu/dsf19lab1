package edu.uprm.cse.datastructures.bag;

public class DynamicBag<E> implements Bag<E>{

	private E[] elements;
	private int currentSize;
	private static final int DEFAULT_SIZE = 10;

	@SuppressWarnings("unchecked")
	public DynamicBag(int initialSize) {
		if (initialSize < 1) {
			throw new IllegalArgumentException("Size must be at least 1");
		}
		this.elements = (E[]) new Object[initialSize];
		this.currentSize = 0;
	}

	public DynamicBag() {
		this(DEFAULT_SIZE);
	}

	@Override
	public int size() {
		 return this.currentSize;
	}

	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	@Override
	public void add(E e) {
		if (e == null) {
			throw new IllegalArgumentException("Argument cannot be null");
		}

		if (this.size() == this.elements.length) {
			this.reAllocate();
		}
		this.elements[this.currentSize++] = e;

	}
    private void reAllocate() {
        @SuppressWarnings("unchecked")
		E temp[] = (E[]) new Object[2*this.size()];
        for (int i=0; i < this.size(); ++i) {
                temp[i] = this.elements[i];
        }
        this.elements = temp;
}
	
	@Override
	public boolean isMember(E e) {
		return this.count(e) > 0;
	}

	@Override
	public boolean remove(E e) {
        for (int i=0; i < this.size(); ++i) {
            if (this.elements[i].equals(e)) {
                    this.elements[i] = this.elements[this.currentSize-1];
                    this.elements[this.currentSize-1] = null;
                    --this.currentSize;
                    return true;
            }
        }
        return false;
	}

	@Override
	public int removeAll(E e) {
        int result = 0;
        while(this.remove(e)) {
                result++;
        }
        return result;

	}

	@Override
	public int count(E e) {
        int result = 0;
        for (int i=0; i < this.size(); ++i) {
                if (this.elements[i].equals(e)) {
                        result++;
                }
        }
        return result;

	}

	@Override
	public void clear() {
        for (int i=0; i < this.size(); ++i) {
            this.elements[i] = null;
        }
        this.currentSize = 0;           
	}

	@Override
	public E[] toArray() {
		@SuppressWarnings("unchecked")
		E result[] = (E[]) new Object[this.size()];
		
		for (int i=0; i < this.size(); ++i) {
			result[i]  = this.elements[i];
		}
		
		return result;
	}

	
	public Bag<E> moreFrequentThan(E e) {
		// make method identify different objects in bag, count them, then compare them
		Bag<E> baggy = new DynamicBag<E>();
		int count = this.helper(e);
		
		for(int i=0; i<this.size(); i++)
		{
			int another = this.helper(this.elements[i]);
			if(count < another)
			{
				if(!baggy.isMember(this.elements[i]))
				{
					baggy.add(this.elements[i]);
				}
			}	
		}
		
		
		return baggy;
	}

	//it counts the amount of times an object appears
	public int helper(Object target) {
		
		int count = 0;
		
		for(int i=0; i<this.size(); i++)
		{
			if(this.elements[i].equals(target))	
				count++;
		}
		
		
		return count;
		
	}




	public Object helperToMoreFrequentThan(Object g) {
		// TODO Auto-generated method stub
		if (this.currentSize == 0) {
			throw new IllegalArgumentException("Empty");
		}
		
		
		
		int maxCount = 0;
		Object maxThing = null;
		for(int j = 0; j<this.size(); j++) 
		{	
			int count = 0;
			Object thing= this.elements[j];
			for(int i=0; i<this.size(); i++)
			{
				if(this.elements[i].equals(thing))	
					count++;
			}
			if(count>maxCount) 
			{
				maxCount = count;
				maxThing = thing;
			}
		}
		
		return maxThing;
	}

}
